package program
/**
 * Created by seven on 18.08.2016.
 */
class Person {
    private String name

    private int age;

    String getName() {
        return name
    }

    void setName(String name) {
        this.name = name
//        this.name = "sarah"
    }

    int getAge() {
        return age
    }

    void setAge(int age) {
        this.age = age
    }

    def getNameAndAge() {
        [name, age]
    }

    boolean asBoolean() {
        age < 120
    }

    @Override
    boolean equals(Object person) {
        this.age.equals(((Person)person).age)
    }
}
