package program
/**
 * Created by seven on 23.08.2016.
 */
class Money {
    String currency;
    int amount;

    Money plus(Money money) {
        int total = this.amount + money.amount;
        new Money(amount: total, currency: currency);
    }

    Money plus(int money) {
        int total = this.amount + money;
        new Money(amount: total, currency: currency);
    }
}
