package program
/**
 * Created by seven on 24.08.2016.
 */
class Equalator {
    public int calculateDuplicatesAuthor(List list, Object item, Closure closure) {
        int count = 0;
        list.each { if (closure(it, item)) count++ }

        return count;
    }
}
