import program.Equalator
import program.Money
import program.Person

/**
 * Created by seven on 18.08.2016.
 */

// getter setter print task
def person1 = new Person(age: 20);

person1?.name = "anna"
println(person1?.name + '\n')

def (name, age) = person1?.getNameAndAge();
println "$name $age"
if (person1) println "is alive\n"

// equals task
def person2 = new Person(age: 20);
if (person1 == person2) println 'equals\n'

// money task
Money money1 = new Money(currency: 'RUB', amount: 120);
Money money2 = new Money(currency: 'RUB', amount: 10);
Money total = money1 + money2 + 300;
println total.amount + '\n'

// list task main
def personList = [new Person(age: 10, name: "kate"), new Person(age: 20, name: "dave"), new Person(age: 30, name: "kate")];
for (def i in 0..<personList.size())
    println personList[i]?.age += 50;

//
println()

// list task author
def personList2 = [new Person(age: 10, name: "kate"), new Person(age: 20, name: "dave"), new Person(age: 30, name: "ilia")];
personList2.each { it.age += 10 }
println personList2

//
println()

// comparator \ author method prints hashcode
println personList.sort({ personA, personB -> personA.name.compareTo(personB.name) ?: personA.age.compareTo(personB.age) })
println personList*.name + personList*.age

//
println()

// equalator task
println new Equalator()
        .calculateDuplicatesAuthor(personList, new Person(name: "kate"), { Person p1, Person p2 -> p1.name == p2.name })

//
println()

// adding properties and methods
Person.metaClass.id = 30;
Person personDaria = new Person(age: 80, name: "daria");
personDaria.metaClass.weight = 60
personDaria.metaClass.sayHello << { println("Hello ${delegate.name}") }
personDaria.sayHello()
println personDaria.getWeight()
println personDaria.getId()